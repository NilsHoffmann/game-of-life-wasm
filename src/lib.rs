mod utils;
use js_sys::Math;
use wasm_bindgen::prelude::*;
use wasm_bindgen::Clamped;
use web_sys::{CanvasRenderingContext2d, ImageData};

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CellStatus {
    Dead = 0,
    Alive = 1,
}

#[wasm_bindgen]
pub struct Universe {
    pub width: u32,
    pub height: u32,
    rules: fn(&CellStatus, u32) -> CellStatus,
    cells: Vec<CellStatus>,
    trails: Vec<u8>,
}

impl Default for Universe {
    fn default() -> Universe {
        let std_rules = |status: &CellStatus, neighbours_alive: u32| {
            return match (status, neighbours_alive) {
                (CellStatus::Alive, x) if x < 2 => CellStatus::Dead,
                (CellStatus::Alive, 2) | (CellStatus::Alive, 3) => CellStatus::Alive,
                (CellStatus::Alive, x) if x > 3 => CellStatus::Dead,
                (CellStatus::Dead, 3) => CellStatus::Alive,
                (status, _) => *status,
            };
        };
        return Universe {
            width: 512,
            height: 512,
            rules: std_rules,
            cells: vec![],
            trails: vec![],
        };
    }
}

#[wasm_bindgen]
impl Universe {
    pub fn cells_ptr(&self) -> *const CellStatus {
        self.cells.as_ptr()
    }

    pub fn trail_prt(&self) -> *const u8 {
        self.trails.as_ptr()
    }

    pub fn map_index_wraparround(&self, i: i32, j: i32) -> Option<usize> {
        let idx = (i % self.width as i32) * self.width as i32 + (j % self.height as i32);
        return Some(idx as usize);
    }

    pub fn map_index(&self, i: i32, j: i32) -> Option<usize> {
        let idx = self.width as i32 * i + j;
        return if idx >= 0 { Some(idx as usize) } else { None };
    }

    pub fn new_random(width: u32, height: u32) -> Universe {
        let mut cells: Vec<CellStatus> = Vec::with_capacity((width * height) as usize);
        let mut trails: Vec<u8> = Vec::with_capacity((width * height) as usize);

        for i in 0..width * height {
            let alive: bool = Math::random() > 0.5;
            cells.push(if alive {
                CellStatus::Alive
            } else {
                CellStatus::Dead
            });
            trails.push(255)
        }
        return Universe {
            width,
            height,
            cells,
            trails,
            ..Universe::default()
        };
    }

    pub fn new_glider(width: u32, height: u32) -> Universe {
        let mut cells: Vec<CellStatus> = Vec::with_capacity((width * height) as usize);
        let mut trails: Vec<u8> = Vec::with_capacity((width * height) as usize);

        for _ in 0..width * height {
            cells.push(CellStatus::Dead);
            trails.push(255)
        }

        let mut u = Universe {
            width,
            height,
            cells,
            trails,
            ..Universe::default()
        };

        let alive = vec![(1, 0), (2, 1), (0, 2), (1, 2), (2, 2)];
        let indices: Vec<usize> = {
            alive
                .iter()
                .map(|(i, j)| u.map_index(*i, *j).unwrap())
                .collect()
        };

        for idx in indices {
            u.cells[idx] = CellStatus::Alive;
        }

        return u;
    }

    fn get_neighbours(&self, i: u32, j: u32) -> u32 {
        let mut total_alive = 0;
        for offset_i in vec![-1i32, 0, 1] {
            for offset_j in vec![-1i32, 0, 1] {
                //skip own cell
                if offset_i == 0 && offset_j == 0 {
                    continue;
                }

                if let Some(cell) = self
                    .map_index(i as i32 + offset_i, j as i32 + offset_j)
                    .and_then(|idx| self.cells.get(idx))
                {
                    total_alive += *cell as u32
                } else {
                    continue;
                }
            }
        }

        return total_alive;
    }

    pub fn forward(&mut self) {
        let mut new_cells = vec![CellStatus::Dead; (self.width * self.height) as usize];

        for i in 0..self.width {
            for j in 0..self.height {
                let surrounding_alive = self.get_neighbours(i, j);
                let idx = self.map_index(i as i32, j as i32).unwrap();
                let cell = self.cells[idx];
                let new_status = (self.rules)(&cell, surrounding_alive);
                //log(&format!("new status {:?}", new_status));
                let new_trail = if new_status == CellStatus::Alive {
                    0
                } else {
                    self.trails[idx].saturating_add(1)
                };

                new_cells[idx] = new_status;
                self.trails[idx] = new_trail
            }
        }
        let alive = new_cells
            .iter()
            .filter(|it| **it == CellStatus::Alive)
            .count();
        log(&format!("Cells alive: {}", alive));
        self.cells = new_cells;
    }
}

#[wasm_bindgen]
#[derive(Copy, Clone, Debug)]
pub struct Colour {
    r: u8,
    g: u8,
    b: u8,
    a: u8,
}
impl Colour {
    pub fn as_vec(&self) -> Vec<u8> {
        return vec![self.r, self.g, self.b, self.a];
    }

    pub fn from_slice(v: &[u8]) -> Colour {
        return Colour {
            r: v[0],
            g: v[1],
            b: v[2],
            a: v[3],
        };
    }
}

#[wasm_bindgen]
pub struct Display {
    ctx: web_sys::CanvasRenderingContext2d,
    colour_str: Vec<String>,
    cell_size: f64,
}

#[wasm_bindgen]
impl Display {
    pub fn render(&self, universe: &Universe) -> Result<(), JsValue> {
        let colours: Vec<&str> = universe
            .trails
            .iter()
            .map(|v| self.colour_str[((*v as usize).min(self.colour_str.len() - 1))].as_ref())
            .collect();

        self.ctx.begin_path();
        for x in 0..universe.width {
            for y in 0..universe.height {
                self.ctx.set_fill_style(&JsValue::from_str(
                    &self.colour_str[(x * universe.width + y) as usize],
                ));

                self.ctx.fill_rect(
                    x as f64 * (self.cell_size) + 1.0,
                    y as f64 * (self.cell_size) + 1.0,
                    self.cell_size,
                    self.cell_size,
                )
            }
        }
        self.ctx.stroke();
        return Ok(());
    }

    pub fn new(ctx: CanvasRenderingContext2d, cell_size: f64, trail_str: String) -> Display {
        return Display {
            ctx,
            cell_size,
            colour_str: trail_str.split(",").map(|s| s.to_owned()).collect(),
        };
    }
}
