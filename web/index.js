import * as wasm from "wasm-game-of-life";
import { memory } from 'wasm-game-of-life/game_of_life_bg'

const COLOURS = {
    grid: "#333333",
    alive: "#ffffff",
    dead: "#000000",

    //https://jdherman.github.io/colormap/
    trail: [
        "#ffffff",
        "#c900df",
        "#b90fe8",
        "#aa1ef1",
        "#9a2df9",
        "#8a38ff",
        "#7a3bff",
        "#6a3dff",
        "#5a40ff",
        "#4c46fc",
        "#3f4ff4",
        "#3259ec",
        "#2563e4",
        "#1b56ca",
        "#1241ac",
        "#092c8d",
        "#00176e",
        "#000000"]
}


const display = {
    /** @type {HTMLCanvasElement} */
    canvas: undefined,
    /** @type {CanvasRenderingContext2D} */
    ctx: undefined,
    universe: undefined,
    cell_size: undefined,
    with_grid: true,

    init: function (canvas, universe) {
        this.canvas = canvas
        this.universe = universe
        this.ctx = canvas.getContext("2d");


        const on_resize = () => {
            const universe_ratio = universe.width / universe.height
            const canvas_ratio = canvas.offsetWidth / canvas.offsetHeight

            if (universe_ratio > canvas_ratio) {
                this.canvas.style.width = "80%"
                this.canvas.width = this.canvas.offsetWidth
                this.cell_size = ((this.canvas.width - universe.width) / universe.width) - 1

                this.canvas.height = universe.height / universe_ratio
                this.canvas.style.height = this.canvas.height + "px"
            } else {
                this.canvas.style.height = '80%'
                this.canvas.height = this.canvas.offsetHeight
                this.cell_size = ((this.canvas.height - universe.height) / universe.height) - 1

                this.canvas.width = this.canvas.height * universe_ratio
                this.canvas.style.width = this.canvas.height + "px"
            }
            console.log(`redrawing at ${canvas.height} x ${canvas.width}. Cell size is ${this.cell_size}`)
            if (this.cell_size <= 3) {
                this.cell_size = this.canvas.height / this.universe.height
                this.with_grid = false
                console.log("disabling grid")
            } else {
                this.with_grid = true
            }
            this.draw_grid()
            this.draw_cells(this.universe)
        }
        on_resize()
        window.onresize = on_resize
    },

    draw_grid: function () {
        if (!this.with_grid) return;
        this.ctx.beginPath();
        this.ctx.strokeStyle = COLOURS.grid;

        for (let i = 0; i <= universe.width; i++) {
            this.ctx.moveTo(i * (this.cell_size + 1) + 1, 0)
            this.ctx.lineTo(i * (this.cell_size + 1) + 1, (this.cell_size + 1) * universe.height + 1)
        }
        for (let j = 0; j <= universe.height; j++) {
            this.ctx.moveTo(0, j * (this.cell_size + 1) + 1)
            this.ctx.lineTo((this.cell_size + 1) * universe.height + 1, j * (this.cell_size + 1) + 1)
        }
        this.ctx.stroke()
    },

    draw_cell(i, j) {
        if (this.with_grid) {
            this.ctx.fillRect(
                i * (this.cell_size + 1) + 1,
                j * (this.cell_size + 1) + 1,
                this.cell_size,
                this.cell_size)
        } else {
            this.ctx.fillRect(
                i * (this.cell_size) + 1,
                j * (this.cell_size) + 1,
                this.cell_size,
                this.cell_size)

        }
    },

    draw_cells: function (universe) {
        this.ctx.beginPath()
        for (let i = 0; i < universe.width; i++) {
            for (let j = 0; j < universe.height; j++) {
                let idx = universe.map_index(i, j)
                let cell = universe.cells[idx]

                this.ctx.fillStyle = cell ? COLOURS.alive : COLOURS.dead
                this.draw_cell(i, j)
            }
        }
        this.ctx.stroke();
    },

    draw_trails: function (universe) {
        this.ctx.beginPath()

        this.ctx.strokeStyle = COLOURS.dead

        if (this.with_grid) {
            this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)
            this.draw_grid()

        } else {
            this.ctx.fillRect(0, 0, universe.width * this.cell_size, universe.height * this.cell_size)
        }

        this.ctx.stroke()

        for (let i = 0; i < universe.width; i++) {
            for (let j = 0; j < universe.height; j++) {
                let trail = universe.trails[universe.map_index(i, j)]
                let cutoff = Math.min(trail, COLOURS.trail.length - 1)
                this.ctx.fillStyle = COLOURS.trail[cutoff]
                this.draw_cell(i, j)
            }
        }
        this.ctx.stroke();

    }
}

const draw_step = () => {
    let time = performance.now()
    universe.forward()

    console.log(`timestep took ${performance.now() - time} ms`)

    time = performance.now()
    display.draw_trails(universe)
    console.log(`drawing ${performance.now() - time} ms`)
}

const animate = (t) => {
    if (t - last_frame > speed) {
        draw_step()
        last_frame = t
    }
    handle = requestAnimationFrame(animate)
}


const canvas = document.getElementById("game-of-life-canvas")
wasm.set_panic_hook()

let universe = wasm.Universe.new_glider(256, 256)
universe.cells = new Uint8Array(memory.buffer, universe.cells_ptr(), universe.width * universe.height)
universe.trails = new Uint8Array(memory.buffer, universe.trail_prt(), universe.width * universe.height)


display.init(canvas, universe)

document.getElementById("forward").onclick = draw_step

let playing = false;
let handle = null;
let play_btn = document.getElementById("play")

let speed = 33
let last_frame = 0
let speed_btn = document.getElementById("speed")

play_btn.onclick = (_) => {
    playing = !playing;
    play_btn.innerHTML = playing ? "Pause" : "Play"

    if (playing) {
        handle = window.requestAnimationFrame(animate)
    } else {
        cancelAnimationFrame(handle)
    }
}

speed_btn.onchange = (_) => {
    speed = speed_btn.value
}

let new_btn = document.getElementById("random")
let width_btn = document.getElementById("w")

new_btn.onclick = ()=> {
    let w = Math.max(1, parseInt(width_btn.value))
    universe = wasm.Universe.new_random(w, w)
    universe.cells = new Uint8Array(memory.buffer, universe.cells_ptr(), universe.width * universe.height)
    universe.trails = new Uint8Array(memory.buffer, universe.trail_prt(), universe.width * universe.height)
    display.init(canvas, universe)
}

